package com.spring_db.mysql.service;

import com.spring_db.mysql.dao.UserDAO;
import com.spring_db.mysql.domain.User;
import com.spring_db.mysql.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @Author Goddy
 * @Date Create in 下午8:45 2018/1/16
 */
@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDAO userDAO;

    public void save() {
        User user = new User();
        user.setName("Jesslyn");
        user.setAge(30);
        user.setGender("girl");
        userRepository.save(user);

        user = new User();
        user.setName("Goddy");
        user.setAge(22);
        user.setGender("boy");
        userRepository.save(user);
    }

    public void findbyAge() {
        PageRequest request = new PageRequest(0, 1);
        userRepository.findByAge(30, request)
                .getContent()
                .stream()
                .map(e -> e.toString())
                .forEach(System.out::println);
               //.forEach(e -> {log.info(e);});
    }

    public void findJesslyn() {
        List<User> user = userRepository.findJesslyn("Jesslyn");
        user.stream().forEach(e -> log.info(e.toString()));
    }

    public void find2Jesslyn() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", "Jesslyn");
        HashMap<String, Object> result = userDAO.findJesslyn(map);
        log.info(result.toString());
    }

}
