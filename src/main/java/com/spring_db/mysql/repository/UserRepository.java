package com.spring_db.mysql.repository;

import com.spring_db.mysql.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Author Goddy
 * @Date Create in 下午8:28 2018/1/16
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Page<User> findByAge(Integer age, Pageable pageable);

    @Query(value = "select * from test_user where name = ?1", nativeQuery = true)
    List<User> findJesslyn(String name);

}
