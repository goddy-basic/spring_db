package com.spring_db.mysql.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;

/**
 * @Author Goddy
 * @Date Create in 下午9:10 2018/1/16
 */
@Mapper
public interface UserDAO {

    @Select("<script>" +
            " select * from test_user " +
            " where name = #{name} " +
            "</script>")
    HashMap<String, Object> findJesslyn(HashMap<String, String> param);

}
