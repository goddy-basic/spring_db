package com.spring_db.mysql.controller;

import com.spring_db.mysql.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 下午9:03 2018/1/16
 */
@RestController
@RequestMapping(value = "/mysql")
public class MysqlController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/test")
    public void test() {
//        userService.save();
        userService.findbyAge();
        userService.findJesslyn();
        userService.find2Jesslyn();
    }

}
