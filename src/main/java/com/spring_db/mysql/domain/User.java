package com.spring_db.mysql.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @Author Goddy
 * @Date Create in 下午8:32 2018/1/16
 */
@Data
@Entity
@Table(name = "test_user")
public class User {

    @Id @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    private String gender;

    private Integer age;

    @Transient
    private String lover;

}
