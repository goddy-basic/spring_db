package com.spring_db.elasticsearch.service;

import com.spring_db.elasticsearch.domain.Customer;
import com.spring_db.elasticsearch.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @Author Goddy
 * @Date Create in 下午6:01 2018/1/16
 */
@Service
@Slf4j
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public void saveCustomers() throws IOException {
        customerRepository.save(new Customer("Jesslyn", "Wu"));
        customerRepository.save(new Customer("Goddy", "Wu"));
    }

    public void fetchAllCustomers() throws IOException {
        log.info("Customers found with findAll():");
        log.info("-------------");
        for (Customer customer : customerRepository.findAll()) {
            log.info(customer.toString());
        }
    }

    public void fetchIndividualCustomers() {
        log.info("Customer found with findByFirstName('Jesslyn')");
        log.info("-------");
        log.info(customerRepository.findByFirstName("Jesslyn").toString());

        log.info("Customers found with findByLastName('Wu')");
        log.info("-------");
        PageRequest pageRequest = new PageRequest(0, 2);
        for (Customer customer : customerRepository.findByLastName("Wu", pageRequest).getContent()) {
            log.info(customer.toString());
        }
    }

}
