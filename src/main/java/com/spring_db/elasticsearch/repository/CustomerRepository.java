package com.spring_db.elasticsearch.repository;

import com.spring_db.elasticsearch.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @Author Goddy
 * @Date Create in 下午5:59 2018/1/16
 */
public interface CustomerRepository extends ElasticsearchRepository<Customer, String> {

    List<Customer> findByFirstName(String firstName);

    Page<Customer> findByLastName(String lastName, Pageable pageable);

}
