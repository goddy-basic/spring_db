package com.spring_db.elasticsearch.domain;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @Author Goddy
 * @Date Create in 下午5:53 2018/1/16
 *
 * @Document:
 * indexName:索引库的名称，个人建议以项目的名称命名
 * type:类型，建议以实体的名称命名
 * shards:默认分区数
 * replicas:每个分区默认的备份数
 * refreshInterval:刷新间隔
 * indexStoreType:索引文件存储类型
 *
 * @Field:
 * filedType:自动检测属性的类型
 * index:默认情况下分词
 * format:
 * pattern:
 * store:默认情况下不存储原文
 * searchAnalyzer:指定字段搜索时使用的分词器
 * indexAnalyzer:指定字段建立索引时指定的分词器
 * ignoreFields:如果某个字段需要被忽略
 * includeInParent:
 */
@Data
@ToString
@Document(indexName = "es-customer", type = "customer", shards = 5, replicas = 1, refreshInterval = "-1")
public class Customer {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    public Customer() {
    }

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
