package com.spring_db.elasticsearch.controller;

import com.spring_db.elasticsearch.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @Author Goddy
 * @Date Create in 下午6:07 2018/1/16
 */
@RestController
@RequestMapping(value = "/elasticsearch")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/test")
    public void test() throws IOException {
//        customerService.saveCustomers();
        customerService.fetchAllCustomers();
        customerService.fetchIndividualCustomers();
    }

}
