package com.spring_db.redis.controller;

import com.spring_db.redis.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 下午8:18 2018/1/16
 */
@RestController
@Slf4j
@RequestMapping(value = "/redis")
public class RedisController {

    @Autowired
    private RedisService redisService;

    @RequestMapping(value = "/test")
    public void save(){
        redisService.save("Favorite", "Jesslyn");
        log.info(redisService.findOne("Favorite").toString());
    }

}
