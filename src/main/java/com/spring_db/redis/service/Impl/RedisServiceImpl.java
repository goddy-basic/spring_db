package com.spring_db.redis.service.Impl;

import com.spring_db.redis.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Author Goddy
 * @Date Create in 下午8:15 2018/1/16
 */
@Service
@Slf4j
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Object findOne(String key) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        if (!redisTemplate.hasKey(key)) {
            log.info("不包含key：{}", key);
        }
        return operations.get(key);
    }

    @Override
    public void save(String key, Object value) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value);
    }

    @Override
    public void save(String key, Object value, long time, TimeUnit timeUnit) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value, time, timeUnit);
    }

    @Override
    public void delete(String key) {
        redisTemplate.delete(key);
    }

}
