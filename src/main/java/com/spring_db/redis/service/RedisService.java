package com.spring_db.redis.service;

import java.util.concurrent.TimeUnit;

/**
 * @Author Goddy
 * @Date Create in 下午8:13 2018/1/16
 */
public interface RedisService {

    /** 通过key查找 */
    Object findOne(String key);

    /** 保存 */
    void save(String key, Object value);

    /** 有效时长保存 */
    void save(String key, Object value, long time, TimeUnit timeUnit);

    /** 删除 */
    void delete(String key);

}
