package com.spring_db.mongo.controller;

import com.spring_db.mongo.service.UserMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 下午10:32 2018/1/16
 */
@RestController
@RequestMapping(value = "/mongo")
public class MongoController {

    @Autowired
    private UserMService userService;

    @RequestMapping(value = "/test")
    public void test() {
//        userService.save();
        userService.findByUsername();
        userService.delete();
        userService.findOne();
    }

}
