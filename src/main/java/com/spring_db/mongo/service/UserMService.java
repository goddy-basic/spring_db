package com.spring_db.mongo.service;

import com.spring_db.mongo.domain.UserM;
import com.spring_db.mongo.repository.UserMRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author Goddy
 * @Date Create in 下午9:24 2018/1/16
 */
@Service
@Slf4j
public class UserMService {

    @Autowired
    private UserMRepository userRepository;

    public void save() {
        userRepository.save(new UserM(1L, "Jesslyn", 30));
        userRepository.save(new UserM(2L, "Goddy", 22));
        userRepository.save(new UserM(3L, "temp", 29));
    }

    public void findOne() {
        log.info(userRepository.findOne(1L).toString());
    }

    public void delete() {
        userRepository.delete(3L);
    }

    public void findByUsername() {
        log.info(userRepository.findByUsername("Jesslyn").toString());
    }

}
