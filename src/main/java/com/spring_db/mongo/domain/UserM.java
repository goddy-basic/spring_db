package com.spring_db.mongo.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @Author Goddy
 * @Date Create in 下午9:21 2018/1/16
 */
@Data
public class UserM {

    @Id
    private Long id;

    private String username;

    private Integer age;

    public UserM(Long id, String username, Integer age) {
        this.id = id;
        this.username = username;
        this.age = age;
    }
}
