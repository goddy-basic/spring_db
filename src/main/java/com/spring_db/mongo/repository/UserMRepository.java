package com.spring_db.mongo.repository;

import com.spring_db.mongo.domain.UserM;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Author Goddy
 * @Date Create in 下午9:22 2018/1/16
 */
public interface UserMRepository extends MongoRepository<UserM, Long> {

    UserM findByUsername(String username);
}
