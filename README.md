#Spring Boot 与各数据库的连接及使用

## Mysql
* 本地安装
* 推荐可视化软件Navicat, Sequel
* 使用方式
  * mybatis
  * Jpa

## elasticsearch
* 本地安装
* 启动
  * cd ~/软件/elasticsearch-2.4.6/bin
  * ./elasticsearch
* 关闭 control + C
* 查询
  * 集群健康 curl 'localhost:9200/_cat/health?v'
  * 节点列表 curl 'localhost:9200/_cat/nodes?v'
  * 索引列表 curl 'localhost:9200/_cat/indices?v'
* 创建索引 curl -XPUT 'localhost:9200/customer?pretty'
* 删除索引 curl -XDELETE 'localhost:9200/customer?pretty'

## mongo
* sudo mongod
* cd /usr/local/Cellar/mongodb/3.4.10/bin
* ./mongo
* use admin
* db.auth("root","^aTFYU23Aqwe^")
* use test
* 关闭 db.shutdownServer()

## redis
* 启动 redis-server
* 进入 redis-cli -h 127.0.0.1 -p 6379  '本地直接redis-cli'
* 关闭 shutdown

## neo4j
* ???
